#include "gear.h"

#include "flow.h"
#include "generator.h"

namespace shdb {

using llvm::CmpInst;

void Gear::insert_input(Gear *gear)
{
    auto *old = input;
    input = gear;
    gear->output = this;
    gear->input = old;
    if (old) {
        old->output = gear;
    }
}

Output::Output(Jit &jit, std::shared_ptr<RowsetAccessor> rowset_accessor, std::shared_ptr<RowsAccessor> rows_accessor)
    : jit(jit), rowset_accessor(std::move(rowset_accessor)), rows_accessor(std::move(rows_accessor))
{}

void Output::produce()
{
    input->produce();
}

void Output::consume(const JitRow &rowin)
{
    auto *row = rowset_accessor->allocate_row();
    rows_accessor->store_row(rowin, row);
}


// Your code goes here

}    // namespace shdb
