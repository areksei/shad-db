#pragma once

#include "cache.h"
#include "file.h"
#include "page.h"
#include "statistics.h"

#include <memory>

namespace shdb {

using FrameIndex = int;

class FramePool
{
    struct Frame
    {
        uint8_t *data = nullptr;
        std::shared_ptr<File> file;
        PageIndex page_index = 0;
        size_t ref_count = 0;
    };

    uint8_t *data;
    std::vector<Frame> frames;
    ClockCache<PageId, FrameIndex> cache;
    std::shared_ptr<Statistics> statistics;

public:
    FramePool(std::shared_ptr<Statistics> statistics, FrameIndex frame_count);
    ~FramePool();
    FramePool(const FramePool &other) = delete;

    void dump_frame(Frame &frame);
    std::pair<FrameIndex, uint8_t *> acquire_frame(std::shared_ptr<File> file, PageIndex page_index);
    void release_frame(FrameIndex frame_index);
};

class Frame
{
    std::shared_ptr<FramePool> frame_pool;
    size_t frame_index;
    uint8_t *data;

public:
    Frame(std::shared_ptr<FramePool> frame_pool, FrameIndex frame_index, uint8_t *data);
    ~Frame();
    uint8_t *get_data();
};

class BufferPool
{
    std::shared_ptr<FramePool> frame_pool;
    friend class Frame;

public:
    BufferPool(std::shared_ptr<Statistics> statistics, FrameIndex frame_count);
    std::shared_ptr<Frame> get_page(std::shared_ptr<File> file, PageIndex page_index);
};

}    // namespace shdb
