#pragma once

#include <cstdint>
#include <queue>
#include <random>
#include <unordered_set>
#include <vector>

#include "message.h"

// IRetrier implements retrying outgoing messages.
// It is necessary to retry outgoing messages because messages could be lost by
// network.
//
// Every schedule()d message is retried until cancel() for that message is
// called.
class IRetrier {
public:
  using Handle = int64_t;
  static constexpr Handle UNDEFINED_HANDLE = -1;

  virtual ~IRetrier();

  // Get messages ready for sending.
  virtual void get_ready(Timestamp timestamp,
                         std::vector<Message> *messages) = 0;

  // Send this message with next send_ready() call.
  virtual void send_once(Timestamp timestamp, Message out) = 0;

  // Send this message with next send_ready() call.
  // Returns message handle.
  //
  // Also, keep sending it until cancel(handle) is called.
  virtual Handle schedule(Timestamp timestamp, Message out) = 0;

  // Forget message with the provided handle.
  virtual void cancel(Handle handle) = 0;
};

// RetrierNoRetries only sends every message once, without retries.
// It is only used in tests where no messages are lost.
class RetrierNoRetries : public IRetrier {
public:
  void get_ready(Timestamp timestamp, std::vector<Message> *messages) override;
  void send_once(Timestamp timestamp, Message out) override;
  Handle schedule(Timestamp timestamp, Message out) override;
  void cancel(Handle handle) override;

private:
  std::vector<Message> out_;
};

// RetrierExpBackoff uses randomized exponential backoff as a retry strategy.
class RetrierExpBackoff : public IRetrier {
public:
  explicit RetrierExpBackoff(int seed);

  void get_ready(Timestamp timestamp, std::vector<Message> *messages) override;
  void send_once(Timestamp timestamp, Message out) override;
  Handle schedule(Timestamp timestamp, Message out) override;
  void cancel(Handle handle) override;

private:
  struct Entry {
    Timestamp ready_ts;
    Handle handle;
    int iteration;

    Message msg;
  };

  struct EntryByTimestamp {
    bool operator()(const Entry &, const Entry &) const;
  };

  int64_t backoff_time(int iteration);

  Handle next_handle_{1};
  std::unordered_set<Handle> canceled_;
  std::priority_queue<Entry, std::vector<Entry>, EntryByTimestamp> queue_;

  std::minstd_rand0 gen_;
  std::normal_distribution<double> gauss_;
};
