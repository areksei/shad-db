#pragma once

// ServerTransaction implements server-side of a transaction.
// Transactions are allowed to span multiple servers: in that case,
// on each server, there will be its own ServerTransaction object for the
// given transaction.

#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "message.h"
#include "retrier.h"

class Storage;

enum class ServerTransactionState {
  // Blank transaction, before Start message is received. Ephemeral state.
  NOT_STARTED,

  // Start is received, serving Get and Put requests.
  // read_timestamp is assigned.
  OPEN,

  // Prepare message is received by the coordinator, and transaction is
  // ready to commit. Non-coordinator state only.
  // commit_timestamp is received from coordinator.
  PREPARED,

  // Rollback message is received by the client, and transaction has been rolled
  // back.
  ROLLBACK,

  // Upon receiving Commit or Prepare, this transaction detected a conflict with
  // another transaction, and has to rollback.
  ROLLED_BACK_BY_SERVER,

  // Received Commit message from the client. Assigned commit_timestamp
  // Sent Prepare message to all other participants. Waiting each participant to
  // reply either PrepareAck, or Conflict.
  COORDINATOR_COMMITTING,

  // Transaction is committed.
  COMMITTED,
};

std::string format_server_transaction_state(ServerTransactionState state);

class ServerTransaction {
public:
  ServerTransaction(ActorId self, Storage *storage, IRetrier *rt);

  void tick(Timestamp ts, const std::vector<Message> &messages,
            std::vector<Message> *msg_out);

  // id of this transaction.
  // Different ServerTransaction objects have the same txid, when they
  // run on different servers, but belong to the same transaction.
  TransactionId txid() const { return txid_; }

  ActorId id() const { return self_; }
  ServerTransactionState state() const { return state_; }

private:
  void process_message_not_started(Timestamp ts, const Message &msg);
  void process_message_open(Timestamp ts, const Message &msg);
  void process_message_rollback(Timestamp ts, const Message &msg);
  void process_message_coordinator_committing(Timestamp ts, const Message &msg);
  void process_message_commited(Timestamp ts, const Message &msg);
  void process_message_rolled_back_by_server(Timestamp ts, const Message &msg);
  void process_message_prepared(Timestamp ts, const Message &msg);

  void report_unexpected_msg(const Message &msg);

  const ActorId self_;
  Storage *storage_;
  IRetrier *rt_;

  ServerTransactionState state_{ServerTransactionState::NOT_STARTED};
  TransactionId txid_{-1};
  ActorId client_{-1};

  Timestamp read_timestamp_;
  Timestamp commit_timestamp_;

  // TODO: add state related to ServerTransaction functioning.
};
