#pragma once

// ClientTransactionParcitipant runs Start, Get and Put within a transaction.
// For a transaction, there is exactly one ClientTransactionParcitipant for a
// single Server.

#include <unordered_map>

#include "client_transaction_results.h"
#include "retrier.h"
#include "types.h"

class ClientTransactionParticipant {
public:
  // Create ClientTransactionParcitipant for a given (client, server).
  ClientTransactionParticipant(ActorId client, ActorId server, IRetrier *rt);

  // Start in coordinator mode. Ask the server for read_timestamp
  void start(Timestamp ts);

  // Start in non-coordinator mode, with provided txid and read_timestamp.
  void start_at_timestamp(Timestamp ts, TransactionId txid,
                          Timestamp read_timestamp);

  // Add get/put request to the transaction.
  void issue_get(Key key);
  void issue_put(Key key, Value value);

  void process_incoming(Timestamp ts, const std::vector<Message> &messages);
  void maybe_issue_requests(Timestamp ts);

  TransactionId txid() const { return txid_; }
  Timestamp read_timestamp() const { return read_timestamp_; }
  bool is_open() const { return state_ == OPEN; }

  int completed_gets() const { return completed_gets_; }
  int completed_puts() const { return completed_puts_; }

  void
  export_results(std::vector<ClientTransactionResults::KeyValue> *gets,
                 std::vector<ClientTransactionResults::KeyValue> *puts) const;

private:
  enum State {
    NOT_STARTED,
    START_SENT,
    OPEN,
  };

  struct RequestState {
    enum State { NOT_STARTED, STARTED, COMPLETED };

    State request_state{NOT_STARTED};
    IRetrier::Handle rt_handle{IRetrier::UNDEFINED_HANDLE};
    Key key;
    Value value;
  };

  void process_replies_start_sent(const std::vector<Message> &messages);
  void process_replies_open(const std::vector<Message> &messages);
  void report_unexpected_msg(const Message &);

  const ActorId client_;
  const ActorId target_;
  IRetrier *rt_;

  IRetrier::Handle start_handle_{IRetrier::UNDEFINED_HANDLE};

  TransactionId txid_{UNDEFINED_TRANSACTION_ID};
  Timestamp read_timestamp_{UNDEFINED_TIMESTAMP};

  State state_{NOT_STARTED};
  std::vector<RequestState> get_state_;
  std::vector<RequestState> put_state_;

  // get_request_, put_request_ are indices from request ID into
  // get_state_, put_state_ correspondingly.
  std::unordered_map<RequestId, size_t> get_request_;
  std::unordered_map<RequestId, size_t> put_request_;

  size_t next_get_{0};
  size_t next_put_{0};

  int completed_gets_{0};
  int completed_puts_{0};
};
