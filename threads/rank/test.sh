#!/bin/bash

set -e
set -x

usernames=(
  "kvk1920"
  "sgjurano"
  "semin-serg"
  "pew-pew"
  "mkspopov"
  "SYury"
  "yuryalekseev"
  "cezarnik"
  "quid"
  "s-reznick"
  "vart"
  "dnorlov"
)

ulimit -n 64000
make -C shad-db/threads tester

port=1300

for run in `seq 1 4`
do
  for testid in `seq 1 6`
  do
    for username in ${usernames[@]}
    do
      skip="logs/${username}.${run}.${testid}.skip"
      stderr="logs/${username}.${run}.${testid}.stderr"
      stdout="logs/${username}.${run}.${testid}.stdout"
      fail="logs/${username}.${run}.${testid}.fail"
      if ./blacklist.py ${username} ${testid}
      then
        touch $skip
      else
        let port=port+1
        binary="bins/${username}"
        spec="shad-db/threads/performance/test.${testid}.json"
        timeout 600 shad-db/threads/tester ${binary} ${port} $spec 2> $stderr > $stdout || touch $fail
        rm -f /tmp/state.db
        sleep 5
      fi
    done
  done
done
