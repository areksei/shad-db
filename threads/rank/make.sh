
set -e
set -x

usernames=(
  "kvk1920"
  "sgjurano"
  "semin-serg"
  "pew-pew"
  "mkspopov"
  "SYury"
  "yuryalekseev"
  "cezarnik"
  "quid"
  "s-reznick"
  "vart"
  "dnorlov"
)

for username in ${usernames[@]}
do
  cp ${username}/threads/main.cpp shad-db/threads/main.cpp
  make -C shad-db/threads main
  cp shad-db/threads/main bins/${username}
done
